#pragma once

#include <QtGlobal>

#ifdef _WIN32
#include <intrin.h>
#else
#include <x86intrin.h>
#endif

#define GRAY    "\033[00;37m"
#define WHITE   "\033[01;37m"
#define RESET   "\033[0m"

template<const char * const*FUNCTION>
struct CycleCounter
{
    const size_t start;
    CycleCounter() : start(__rdtsc()) {}
    ~CycleCounter() {
        const int64_t end = __rdtsc();
        if (qEnvironmentVariableIsSet("QEH_BENCHMARK")) {
            printf(WHITE "\t%s: %zu\n" RESET, *FUNCTION, end - start);
        }
    }
};

// Dirty trick for removing runtime overhead
#define COUNT_BLOCK_CYCLES \
    static constexpr const char *FUNCTION = __PRETTY_FUNCTION__; \
    CycleCounter<&FUNCTION> cycleCounter;
