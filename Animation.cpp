/*
 * SPDX-FileCopyrightText: 2021 Martin Sandsmark <martin.sandsmark@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "Animation.h"

#ifdef ENABLE_NSGIF
extern "C" {
#include <libnsgif.h>
}
namespace {
#define BYTES_PER_PIXEL 4
#define MAX_IMAGE_BYTES (48 * 1024 * 1024)

    static void *bitmap_create(int width, int height) {
        if (((long long)width * (long long)height) > (MAX_IMAGE_BYTES/BYTES_PER_PIXEL)) {
            return nullptr;
        }
        return calloc(width * height, BYTES_PER_PIXEL);
    }

    static void bitmap_set_opaque(void *, bool ) { }
    static bool bitmap_test_opaque(void *) { return false; }
    static unsigned char *bitmap_get_buffer(void *bitmap) { return (uchar*)bitmap; }
    static void bitmap_destroy(void *bitmap) { free(bitmap); }
    static void bitmap_modified(void *) { }
}
#endif//ENABLE_NSGIF

Animation::Animation(QObject *parent) : QObject(parent)
{
    reader.setAutoTransform(true);
    m_timer.setSingleShot(true);
    connect(&m_timer, &QTimer::timeout, this, &Animation::onTimeout);
}

int Animation::frameCount() {
    if (m_cacheFull) {
        return imageCache.count();
    }
    if (m_brokenFile) {
        return m_largestImageNumber;
    }
    int count = reader.imageCount();
    if (count <= 0) {
        return m_largestImageNumber;
    }
    return count;
}

void Animation::checkGif(QIODevice *device) {
#ifdef ENABLE_NSGIF
    if (!device || !device->isReadable()) {
        return;
    }
    QByteArray data = device->read(5);
    if (data != "GIF89") {
        return;
    }
    data += device->readAll();

    gif_animation gif;
    gif_bitmap_callback_vt bitmap_callbacks = {
        bitmap_create,
        bitmap_destroy,
        bitmap_get_buffer,
        bitmap_set_opaque,
        bitmap_test_opaque,
        bitmap_modified
    };
    gif_create(&gif, &bitmap_callbacks);
    gif_result result = gif_initialise(&gif, data.size(), (uchar*)data.data());
    if (result != GIF_OK && result != GIF_WORKING) {
        qWarning() << "Gif open failed";
        gif_finalise(&gif);
        return;
    }

    result = gif_decode_frame(&gif, 0);
    if (result != GIF_OK && result != GIF_WORKING) {
        qWarning() << "Gif read frame failed";
        gif_finalise(&gif);
        return;
    }

    int delay = gif.frames[0].frame_delay;
    // workaround for qt shenanigans
    if (delay < 2 && delay >= 0) {
        m_delayOverride = delay * 10;
        qDebug() << "Overriding delay to" << m_delayOverride;
    }

    gif_finalise(&gif);
#else//ENABLE_NSGIF
    Q_UNUSED(device);
#endif//ENABLE_NSGIF
}

const QImage &Animation::load(QIODevice *device) {
    m_timer.stop();
    state = Paused;

    if (reader.device()) {
        reader.device()->deleteLater();
    }

    m_delayOverride = -1;
    m_cacheFull = false;
    m_brokenFile = false;
    reader.setDevice(device);
    currentImage = QImage();
    imageCache.clear();
    m_delayCache.clear();
    m_delay.invalidate();

    m_currentFrame = 0;
    m_largestImageNumber = 0;
    m_timeLeft = 0;

    if (device) {
        if (!device->isSequential()) {
            qint64 origPos = device->pos();
            checkGif(device);
            device->seek(origPos);
        }

        currentImage = reader.read();
        m_timeLeft = reader.nextImageDelay();

        if (cacheFrames && !currentImage.isNull()) {
            imageCache.append(currentImage);
            m_delayCache.append(m_timeLeft);
        }
    }

    return currentImage;
}
void Animation::jumpToImage(const int num) {
    if (num == m_currentFrame + 1) {
        m_timer.stop();
        if (!readNextImage(false)) {
            handleReaderError(false);
        }
        return;
    }
    m_currentFrame = num;
    if (cacheFrames) {
        if (m_cacheFull) {
            if (m_currentFrame >= imageCache.size()) {
                m_currentFrame = imageCache.size() - 1;
            }
        }
        if (m_currentFrame < imageCache.size()) {
            currentImage = imageCache[m_currentFrame];
            emit nextFrameReady(currentImage);
            return;
        }
    }
    if (!reader.jumpToImage(num)) {
        qWarning() << "Failed to jump to" << num;
    }
}
void Animation::start() {
    state = Playing;
    m_timer.start(qMax(m_timeLeft, 0));
    m_timeLeft = 0;
}

void Animation::pause() {
    state = Paused;
    m_timeLeft = m_timer.remainingTime();
    m_timer.stop();
    m_delay.invalidate();
}

bool Animation::readNextImage(bool startTimer) {
    if (currentImage.isNull()) {
        if (m_currentFrame != 0) {
            qWarning() << "Invalid state";
            return false;
        }
        currentImage = reader.read();
        if (currentImage.isNull()) {
            qWarning() << "Failed to read first frame" << reader.errorString();
            emit error(reader.errorString());
            return false;
        }
    }
    QElapsedTimer t; t.start();

    currentImage = QImage();

    if (cacheFrames) {
        if (m_cacheFull && m_currentFrame >= imageCache.size()) {
            m_currentFrame = 0;
        }
        if (m_currentFrame < imageCache.size()) {
            Q_ASSERT(imageCache.size() == m_delayCache.size());
            if (!imageCache[m_currentFrame].isNull()) {
                currentImage = imageCache[m_currentFrame];
                setNextTimer(m_delayCache[m_currentFrame], t.elapsed());
                m_currentFrame++;
                return true;
            } else {
                qWarning() << "Null in cache!";
            }
        }
    }

    if (m_cacheFull) {
        qWarning() << "We shouldn't get here";
    }

    if (reader.canRead()) {
        currentImage = reader.read();
    }

    if (currentImage.isNull()) {
        return false;
    }

    int readerImageNumber = reader.currentImageNumber();
    if (readerImageNumber < 0) {
        qWarning() << "Bad image number" << readerImageNumber;
        readerImageNumber = m_currentFrame + 1;
    }

    const int timeToNext = qMax(reader.nextImageDelay(), 0);

    if (cacheFrames) {
        if (m_currentFrame >= imageCache.size()) {
            imageCache.resize(m_currentFrame + 1);
        }
        imageCache[m_currentFrame] = currentImage;

        if (m_currentFrame >= m_delayCache.size()) {
            m_delayCache.resize(m_currentFrame + 1);
        }
        m_delayCache[m_currentFrame] = timeToNext;
    }

    if (startTimer) {
        setNextTimer(timeToNext, t.elapsed());
    }

    int count = reader.imageCount();
    if ((count > 0 && readerImageNumber >= count) || readerImageNumber < m_currentFrame) {
        if (cacheFrames) {
            m_cacheFull = true;
        } else {
            if (!reader.jumpToImage(0)) {
                qWarning() << "Failed to seek to start";
                return false;
            }
            m_currentFrame = 0;
        }
    } else {
        m_cacheFull = false;
        m_currentFrame++;
    }
    m_largestImageNumber = qMax(m_currentFrame, m_largestImageNumber);
    return true;
}

void Animation::handleReaderError(bool startTimer) {
    QElapsedTimer t; t.start();
    if (!cacheFrames || imageCache.isEmpty() || imageCache.first().isNull()) {
        if (m_currentFrame <= 0) {
            emit error(reader.errorString());
            qWarning() << "Reader failed and we haven't read any frames, giving up";
            return;
        }

        qDebug() << "Cache not enabled, trying to reset";
        m_brokenFile = true;
        m_currentFrame = 0;
        QIODevice *device = reader.device();
        reader.setDevice(nullptr);
        device->seek(0);
        reader.setDevice(device);

        if (!readNextImage(startTimer)) {
            qWarning() << "Really failed to read";
            QImage image = currentImage;
            QString errorMessage = reader.errorString();
            load(nullptr);
            currentImage = image;
            emit error(errorMessage);
        }
        return;
    }

    m_cacheFull = true;
    m_currentFrame = 0;
    currentImage = imageCache[m_currentFrame];
    if (startTimer) {
        setNextTimer(m_delayCache[m_currentFrame], t.elapsed());
    }
}

void Animation::setNextTimer(int delay, int processingTime) {
    if (m_delayOverride >= 0) {
        delay = m_delayOverride;
    }

    if (m_delay.isValid()) {
        processingTime = qMax<qint64>(processingTime, m_delay.elapsed() - m_timer.interval());
    }
    m_delay.restart();

    delay -= processingTime;
    speed = qBound(1, speed, 1000);
    delay = 100 * delay / speed;
    m_timer.setInterval(qMax(delay, 0));
}
