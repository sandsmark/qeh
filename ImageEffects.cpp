//Mostly taken from Blitz
//
//   Copyright (c) 2003-2007 Clarence Dang <dang@kde.org>
//   Copyright (c) 2006 Mike Gashler <gashlerm@yahoo.com>
//   All rights reserved.

//   Redistribution and use in source and binary forms, with or without
//   modification, are permitted provided that the following conditions
//   are met:

//   1. Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//   2. Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.

//   THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//   IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//   IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//   NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef IMGEFFECTS_H
#define IMGEFFECTS_H

#include <QDebug>
#include <QImage>
#include <limits>
#include "CycleCounter.h"

inline QRgb convertFromPremult(QRgb p)
{
    int alpha = qAlpha(p);
    return (!alpha ? 0 : qRgba(255 * qRed(p) / alpha,
                               255 * qGreen(p) / alpha,
                               255 * qBlue(p) / alpha,
                               alpha));
}

inline QRgb convertToPremult(QRgb p)
{
    unsigned int a = p >> 24;
    unsigned int t = (p & 0xff00ff) * a;
    t = (t + ((t >> 8) & 0xff00ff) + 0x800080) >> 8;
    t &= 0xff00ff;

    p = ((p >> 8) & 0xff) * a;
    p = (p + ((p >> 8) & 0xff) + 0x80);
    p &= 0xff00;
    p |= t | (a << 24);
    return (p);
}

// These are used as accumulators

typedef struct {
    quint32 red, green, blue, alpha;
} IntegerPixel;

typedef struct {
    quint16 red, green, blue, alpha;
} ShortPixel;

typedef struct {
    quint32 red, green, blue, alpha;
} HistogramListItem;

template<typename T>
bool actualNormalize(QImage &img)
{
    static constexpr T numColors = std::numeric_limits<T>::max();
    static constexpr int bucketSize = std::numeric_limits<T>::max() + 1;

    COUNT_BLOCK_CYCLES;

    if (img.isNull()) {
        return (false);
    }

    IntegerPixel intensity;
    HistogramListItem *histogram;
    ShortPixel *normalize_map;
    IntegerPixel high, low;

    uint threshold_intensity;
    int i, count;
    QRgb pixel, *dest;
    unsigned short r, g, b;

    count = img.width() * img.height();

    histogram = new HistogramListItem[bucketSize];
    normalize_map = new ShortPixel[bucketSize];

    // form histogram
    memset(histogram, 0, bucketSize * sizeof(HistogramListItem));
    dest = (QRgb *)img.bits();

    const int height = img.height();
    const int width = img.width();

    for (int y = 0; y < img.height(); y++) {
        const T *source = (const T *)img.constScanLine(y);

        for (int x = 0; x < width; x++) {
            histogram[source[x * 4 + 0]].red++;
            histogram[source[x * 4 + 1]].green++;
            histogram[source[x * 4 + 2]].blue++;
            histogram[source[x * 4 + 3]].alpha++;
        }
    }

    // find the histogram boundaries by locating the .01 percent levels.
    //threshold_intensity = count/100;
    threshold_intensity = count / 500;

    memset(&intensity, 0, sizeof(IntegerPixel));

    for (low.red = 0; low.red < bucketSize; ++low.red) {
        intensity.red += histogram[low.red].red;

        if (intensity.red > threshold_intensity) {
            break;
        }
    }

    memset(&intensity, 0, sizeof(IntegerPixel));

    for (high.red = bucketSize; high.red-- > 0;) {
        intensity.red += histogram[high.red].red;

        if (intensity.red > threshold_intensity) {
            break;
        }
    }

    memset(&intensity, 0, sizeof(IntegerPixel));

    for (low.green = low.red; low.green < high.red; ++low.green) {
        intensity.green += histogram[low.green].green;

        if (intensity.green > threshold_intensity) {
            break;
        }
    }

    memset(&intensity, 0, sizeof(IntegerPixel));

    for (high.green = high.red; high.green != low.red; --high.green) {
        intensity.green += histogram[high.green].green;

        if (intensity.green > threshold_intensity) {
            break;
        }
    }

    memset(&intensity, 0, sizeof(IntegerPixel));

    for (low.blue = low.green; low.blue < high.green; ++low.blue) {
        intensity.blue += histogram[low.blue].blue;

        if (intensity.blue > threshold_intensity) {
            break;
        }
    }

    memset(&intensity, 0, sizeof(IntegerPixel));

    for (high.blue = high.green; high.blue != low.green; --high.blue) {
        intensity.blue += histogram[high.blue].blue;

        if (intensity.blue > threshold_intensity) {
            break;
        }
    }

    delete[] histogram;

    // stretch the histogram to create the normalized image mapping.
    for (i = 0; i < bucketSize; i++) {
        if (i < low.red) {
            normalize_map[i].red = 0;
        } else {
            if (i > high.red) {
                normalize_map[i].red = numColors;
            } else if (low.red != high.red)
                normalize_map[i].red = (float(numColors) * (i - low.red)) /
                                       (high.red - low.red);
        }

        if (i < low.green) {
            normalize_map[i].green = 0;
        } else {
            if (i > high.green) {
                normalize_map[i].green = numColors;
            } else if (low.green != high.green)
                normalize_map[i].green = (float(numColors) * (i - low.green)) /
                                         (high.green - low.green);
        }

        if (i < low.blue) {
            normalize_map[i].blue = 0;
        } else {
            if (i > high.blue) {
                normalize_map[i].blue = numColors;
            } else if (low.blue != high.blue)
                normalize_map[i].blue = (float(numColors) * (i - low.blue)) /
                                        (high.blue - low.blue);
        }
    }

    // write
    for (int y = 0; y < height; y++) {
        T *target = (T *)img.scanLine(y);

        for (int x = 0; x < width; x++) {
            const T red = target[x * 4 + 0];
            const T green = target[x * 4 + 1];
            const T blue = target[x * 4 + 2];
            target[x * 4 + 0] = low.red != high.red ? normalize_map[red].red : red;
            target[x * 4 + 1] = low.green != high.red ? normalize_map[green].green : green;
            target[x * 4 + 2] = low.blue != high.blue ? normalize_map[blue].blue : blue;
        }
    }

    delete[] normalize_map;
    return (true);
}

bool normalize(QImage &img)
{
    if (img.depth() == 64) {
        if (img.format() == QImage::Format_RGBA64_Premultiplied) {
            img = img.convertToFormat(QImage::Format_RGBA64);
        }

        return actualNormalize<uint16_t>(img);
    } else {
        if (img.depth() != 32 || img.format() == QImage::Format_ARGB32_Premultiplied) {
            img = img.convertToFormat(QImage::Format_ARGB32);
        }

        return actualNormalize<uint8_t>(img);
    }
}

template<typename T>
bool actualEqualize(QImage &img)
{
    static constexpr int numColors = std::numeric_limits<T>::max() + 1;
    COUNT_BLOCK_CYCLES;

    if (img.isNull()) {
        return (false);
    }

    HistogramListItem *histogram;
    IntegerPixel *map;
    IntegerPixel intensity, high, low;
    ShortPixel *equalize_map;
    int i, count;
    unsigned short r, g, b;

    count = img.width() * img.height();

    map = new IntegerPixel[numColors];
    histogram = new HistogramListItem[numColors];
    equalize_map = new ShortPixel[numColors];

    // form histogram
    memset(histogram, 0, numColors * sizeof(HistogramListItem));

    const int height = img.height();
    const int width = img.width();

    for (int y = 0; y < img.height(); y++) {
        const T *source = (const T *)img.constScanLine(y);

        for (int x = 0; x < width; x++) {
            histogram[source[x * 4 + 0]].red++;
            histogram[source[x * 4 + 1]].green++;
            histogram[source[x * 4 + 2]].blue++;
            histogram[source[x * 4 + 3]].alpha++;
        }
    }

    // integrate the histogram to get the equalization map
    memset(&intensity, 0, sizeof(IntegerPixel));

    for (i = 0; i < numColors; ++i) {
        intensity.red += histogram[i].red;
        intensity.green += histogram[i].green;
        intensity.blue += histogram[i].blue;
        map[i] = intensity;
    }

    low = map[0];
    high = map[numColors - 1];
    memset(equalize_map, 0, numColors * sizeof(ShortPixel));

    for (i = 0; i < numColors; ++i) {
        if (high.red != low.red)
            equalize_map[i].red =
                (((numColors - 1.) * (map[i].red - low.red)) / (high.red - low.red));

        if (high.green != low.green)
            equalize_map[i].green =
                (((numColors - 1.) * (map[i].green - low.green)) / (high.green - low.green));

        if (high.blue != low.blue)
            equalize_map[i].blue =
                (((numColors - 1.) * (map[i].blue - low.blue)) / (high.blue - low.blue));
    }

    for (int y = 0; y < height; y++) {
        T *target = (T *)img.scanLine(y);

        for (int x = 0; x < width; x++) {
            const T red = target[x * 4 + 0];
            const T green = target[x * 4 + 1];
            const T blue = target[x * 4 + 2];
            target[x * 4 + 0] = low.red != high.red ? equalize_map[red].red : red;
            target[x * 4 + 1] = low.green != high.red ? equalize_map[green].green : green;
            target[x * 4 + 2] = low.blue != high.blue ? equalize_map[blue].blue : blue;
        }
    }

    delete[] histogram;
    delete[] map;
    delete[] equalize_map;
    return (true);
}

bool equalize(QImage &img)
{
    if (img.depth() == 64) {
        if (img.format() == QImage::Format_RGBA64_Premultiplied) {
            img = img.convertToFormat(QImage::Format_RGBA64);
        }

        return actualEqualize<uint16_t>(img);
    } else {
        if (img.depth() != 32 || img.format() == QImage::Format_ARGB32_Premultiplied) {
            img = img.convertToFormat(QImage::Format_ARGB32);
        }

        return actualEqualize<uint8_t>(img);
    }
}

bool mean(QImage &img)
{
    // basically just a hack, inlined a convolution filter
    static constexpr int matrix_size = 7;
    const unsigned divisor = matrix_size * matrix_size;
    static constexpr int edge = matrix_size / 2;

    static_assert(matrix_size % 2, "kernel width must be an odd number!");

    const int w = img.width();
    const int h = img.height();

    if (w < 3 || h < 3) {
        qWarning("Blitz::convolve(): Image is too small!");
        return false;
    }

    if (img.hasAlphaChannel()) {
        if (img.format() != QImage::Format_ARGB32) {
            img.convertTo(QImage::Format_ARGB32);
        }
    } else if (img.format() != QImage::Format_RGB32) {
        img.convertTo(QImage::Format_RGB32);
    }

    // avoid allocating each time
    static QImage buffer(w, h, img.format());
    if (buffer.width() != w || buffer.height() != h || buffer.format() != img.format()) {
        buffer = QImage(w, h, img.format());
    }
    static const QRgb *scanblock[matrix_size];

    for (int y = 0; y < h; ++y) {
        const QRgb *src = (QRgb *)img.constScanLine(y);
        QRgb *dest = (QRgb *)buffer.scanLine(y);
        // Read in scanlines to pixel neighborhood. If the scanline is outside
        // the image use the top or bottom edge.
        int x = y - edge;

        for (int i = 0; x <= y + edge; ++i, ++x) {
            scanblock[i] = (const QRgb *)
                           img.constScanLine((x < 0) ? 0 : (x > h - 1) ? h - 1 : x);
        }

        // Now we are about to start processing scanlines. First handle the
        // part where the pixel neighborhood extends off the left edge.
        for (x = 0; x - edge < 0 ; ++x) {
            unsigned r = 0, g = 0, b = 0;

            for (int matrix_y = 0; matrix_y < matrix_size; ++matrix_y) {
                const uchar *s = (const uchar*)scanblock[matrix_y];
                int matrix_x = -edge;

                while (x + matrix_x < 0) {
                    ++matrix_x;
                    r += s[2];
                    g += s[1];
                    b += s[0];
                }

                while (matrix_x <= edge) {
                    r += s[2];
                    g += s[1];
                    b += s[0];
                    ++matrix_x;
                    s+=4;
                }
            }

            r /= divisor;
            g /= divisor;
            b /= divisor;
            *dest++ = qRgba(r, g, b, qAlpha(*src++));
        }

        // Okay, now process the middle part where the entire neighborhood
        // is on the image.
        for (; x + edge < w; ++x) {
            unsigned r = 0, g = 0, b = 0;

            for (int matrix_y = 0; matrix_y < matrix_size; ++matrix_y) {
                const uchar *s = (const uchar*)(scanblock[matrix_y] + (x - edge));

                for (int matrix_x = -edge; matrix_x <= edge; ++matrix_x) {
                    r += s[2];
                    g += s[1];
                    b += s[0];
                    s+=4;
                }
            }

            r /= divisor;
            g /= divisor;
            b /= divisor;
            *dest++ = qRgba(r, g, b, qAlpha(*src++));
        }

        // Finally process the right part where the neighborhood extends off
        // the right edge of the image
        for (; x < w; ++x) {
            unsigned r = 0, g = 0, b = 0;
            for (int matrix_y = 0; matrix_y < matrix_size; ++matrix_y) {
                const uchar *s = (const uchar*)(scanblock[matrix_y] + x - edge);
                int matrix_x = -edge;

                while (x + matrix_x < w) {
                    r += s[2];
                    g += s[1];
                    b += s[0];
                    s += 4;
                    ++matrix_x;
                }

                s -= 4;

                while (matrix_x <= edge) {
                    r += s[2];
                    g += s[1];
                    b += s[0];
                    ++matrix_x;
                }
            }

            r /= divisor;
            g /= divisor;
            b /= divisor;
            *dest++ = qRgba(r, g, b, qAlpha(*src++));
        }
    }

    img = buffer;
    return true;
}

#endif // IMGEFFECTS_H

