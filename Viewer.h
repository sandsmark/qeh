/*
 * Main window
 * SPDX-FileCopyrightText: 2021 Martin Sandsmark <martin.sandsmark@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#pragma once

#include "Animation.h"
#include "Histogram.h"

#include <QRasterWindow>
#include <QElapsedTimer>
#include <QImage>
#include <QPixmap>

class QIODevice;

class Viewer : public QRasterWindow
{
    Q_OBJECT

public:
    Viewer();

    bool load(const QString &filename);

    QElapsedTimer *startupTimer = nullptr;

    bool isValid() {
        return !m_animation.currentImageSize().isEmpty() || !m_image.isNull();
    }
    QImageReader::ImageReaderError error() const { return m_error; }

    enum Effect {
        Hue = 0,
        Saturation = 1,
        Value = 2,
        Lightness = 3,
        Hsv = 4,
        None = 5,
        Normalize,
        Equalize,
        YCbCr,
        Mean,
    };
    Q_ENUM(Effect);

private slots:
    void setAspectRatio();
    void resetMovie();

protected:
    void paintEvent(QPaintEvent*) override;
    void keyPressEvent(QKeyEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;
    void moveEvent(QMoveEvent *event) override;
    void mousePressEvent(QMouseEvent *evt) override;
    void mouseReleaseEvent(QMouseEvent *evt) override;
    void mouseMoveEvent(QMouseEvent *evt) override;
    void wheelEvent(QWheelEvent *event) override;
    bool event(QEvent *event) override;

private:
    void toggleEffect(const Effect effect) {
        if (m_effect == effect) {
            m_effect = None;
        } else {
            m_effect = effect;
        }
        updateScaled();
        update();
    }

    void updateSize(QSize newSize, bool initial = false);
    void ensureVisible();
    void updateScaled();
    void loadClipboard();
    void loadNext(bool backwards);

    QImage m_image;
    QImage m_scaled;
    QSize m_imageSize;
    QSize m_scaledSize;
    QImageReader::ImageReaderError m_error = QImageReader::UnknownError;

    Animation m_animation;

    bool m_decodeSuccess = false;
    bool m_needReset = false;

    QPointF m_lastMousePos;
    bool m_brokenFormat = false;

    QString m_fileName;
    QByteArray m_buffer;

    bool m_showInfo = false;
    QString m_format;
    bool m_hasTransforms = false;
    QImageIOHandler::Transformations m_transforms{}; // just for printing

    Effect m_effect = None;

    bool m_showHelp = false;
    bool m_showHistogram = false;

    int m_rotation = 0;

    QElapsedTimer m_delayTimer;

    bool m_smooth = true;
    bool m_fillBackground = false;
    QPixmap m_background;

    Histogram m_histogram;

    // Generated on demand for performance reasons
    QStringList m_fileList;
};

