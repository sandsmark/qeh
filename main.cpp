/*
 * SPDX-FileCopyrightText: 2021 Martin Sandsmark <martin.sandsmark@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "Viewer.h"

extern "C" {
#include <unistd.h>
}

#include <QGuiApplication>
#include <QDebug>
#include <QImageReader>
#include <QFileInfo>
#include <QMimeDatabase>
#include <QAccessible>
#include <QTimer>
#include <QSurfaceFormat>
#include <QElapsedTimer>

#ifdef DEBUG_LAUNCH_TIME
#include "CycleCounter.h"

static int64_t s_startTime;
void __attribute__ ((constructor)) saveStartTime() {
    s_startTime = __rdtsc();
}

void __attribute__ ((destructor)) printStopTime() {
    if (getenv("QEH_BENCHMARK")) {
        printf(WHITE "\tTotal execution: %llu cycles\n" RESET, __rdtsc() - s_startTime);
    }
}
#endif



void dummyAccessibilityRootHandler(QObject*) {  }

static void listFormats()
{
    qDebug() << "Supported formats:";
    QMimeDatabase db;
    for (const QByteArray &format : QImageReader::supportedMimeTypes()) {
        QMimeType mime = db.mimeTypeForName(format);
        if (!mime.isValid() || mime.comment().isEmpty()) {
            qDebug().noquote() << " - " << QImageReader::imageFormatsForMimeType(format).join(", ");
            continue;
        }
        if (mime.filterString().isEmpty()) {
            qDebug().noquote() << " - " << mime.comment() << mime.name();
            continue;
        }
        qDebug().noquote() << " - " << mime.filterString() << mime.name();
    }
}

static void printHelp(const char *app)
{
    qDebug() << "Usage:" << app << "[-l] [filename]";
    qDebug() << "\t-l\tList supported file formats\n";
    qDebug() << "You can also pipe data to it, like so:";
    qDebug() << "   base64 -d foo | qeh";
}


int main(int argc, char *argv[])
{
    qputenv("QT_XCB_GL_INTEGRATION", "none");
    qputenv("QT_QPA_PLATFORMTHEME", "generic");
    qputenv("QT_XCB_NO_XI2_MOUSE", "1");

    // Stole this from the qdbus unit tests, should hopefully kill all attempts
    // at using dbus and slowing is down.
    qputenv("DBUS_SESSION_BUS_ADDRESS", "unix:abstract=/tmp/does_not_exist");
    qputenv("DBUS_SYSTEM_BUS_ADDRESS", "unix:abstract=/tmp/does_not_exist");

    static const bool printTiming = qEnvironmentVariableIsSet("QEH_BENCHMARK");
#ifdef DEBUG_LAUNCH_TIME
    if (printTiming) {
        printf(WHITE "\tCycles before timer created: %llu\n" RESET, __rdtsc() - s_startTime);
    }
#endif

    // Otherwise it tries to contact SPI and stuff
    // But we don't really have any GUI, so we don't need this
    QAccessible::installRootObjectHandler(dummyAccessibilityRootHandler);
    QElapsedTimer t; t.start();

    QString filename;


    if (!isatty(STDIN_FILENO)) {
        filename = "-"; // assume there's a pipe
    } else if (argc != 2) {
        printHelp(argv[0]);
        return 1;
    }

    for (int i=1; i<argc; i++) {
        if (access(argv[i], R_OK) == 0) {
            filename = argv[i];
            // Always prioritize files if they exist
            break;
        }
        // If it starts with a - the user is either using a pipe or trying
        // command line flags.
        if (*argv[i] == '-') {
            switch(argv[i][1]) {
            case '\0': // -: pipe input
                continue;
            case 'l':
                listFormats();
                return 0;
            default:
                printHelp(argv[0]);
                return 1;
            }
        }
        perror(argv[i]);
        return 1;
    }

    QGuiApplication a(argc, argv);

    QSurfaceFormat defaultFormat = QSurfaceFormat::defaultFormat();
    if (!defaultFormat.hasAlpha()) {
        defaultFormat.setAlphaBufferSize(8);
    }
    QSurfaceFormat::setDefaultFormat(defaultFormat);


    QFileInfo info(filename);

    a.setApplicationDisplayName(filename == "-" ? "Pipe" : info.fileName());

    Viewer w;

    if (printTiming) {
        QTimer::singleShot(0, &a, [&]() {
                qDebug() << "Mainloop running:" << t.elapsed() << "ms";
            });
        w.startupTimer = &t;
    }

    if (!w.load(filename)) {
        return 1;
    }
    w.show();

    return a.exec();
}
