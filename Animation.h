/*
 * SPDX-FileCopyrightText: 2021 Martin Sandsmark <martin.sandsmark@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#pragma once

#include <QTimer>
#include <QImageReader>
#include <QElapsedTimer>

struct Animation : public QObject
{
    Q_OBJECT
public:
    enum State {
        Playing,
        Paused
    };
    Q_ENUM(State);
    State state = Paused;

    Animation(QObject *parent = nullptr);
    const QImage &load(QIODevice *device);

    int speed = 100;

    bool cacheFrames = true;

    QVector<QImage> imageCache;
    QVector<int> m_delayCache;

    operator bool() { return reader.device() != nullptr; }
    bool isActive() const { return reader.device() != nullptr; }

    QSize currentImageSize() const { return currentImage.size(); }
    const QImage &lastImage() const { return currentImage; }

    int frameCount();

    void jumpToImage(const int num);
    int currentImageNumber() const { return m_currentFrame; }

    QByteArray format() const { return reader.format(); }


    void onPainted() {
        if (state == Playing && !m_timer.isActive()) {
            if (m_delay.isValid()) {
                m_timer.setInterval(qMax<qint64>(m_timer.interval() - m_delay.elapsed(), 0));
            }
            m_timer.start();
        }
    }
public slots:
    void start();
    void pause();

signals:
    void nextFrameReady(const QImage &image);
    void error(const QString &message);

private slots:
    void onTimeout() {
        if (!readNextImage(true)) {
            handleReaderError(true);
        }

        emit nextFrameReady(currentImage);
    }

private:
    void setNextTimer(int delay, int processingTime);
    void checkGif(QIODevice *device);

    bool readNextImage(bool startTimer);
    void handleReaderError(bool startTimer);

    QTimer m_timer;
    int m_currentFrame = 0;
    int m_largestImageNumber = 0;
    int m_delayOverride = -1;

    int m_timeLeft = 0;
    QImageReader reader;

    bool m_cacheFull = false;

    bool m_brokenFile = false;

    QImage currentImage;

    QElapsedTimer m_delay;
};

