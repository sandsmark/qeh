#pragma once

#include <QImage>
#include <QDebug>

void toYCbCr(QImage *img)
{
    if (img->hasAlphaChannel()) {
        if (img->format() != QImage::Format_ARGB32) {
            img->convertTo(QImage::Format_ARGB32);
        }
    } else if (img->format() != QImage::Format_RGB32) {
        img->convertTo(QImage::Format_RGB32);
    }
    const size_t byteCount = img->sizeInBytes();
    uchar *pixels = img->bits();
    uchar *out = pixels;
    for (size_t i=0; i<byteCount; i+=4) {
        const int r = pixels[i + 2];
        const int g = pixels[i + 1];
        const int b = pixels[i + 0];
        out[i + 2] = ( 66*r + 129*g +  25*b + 128)/256 +  16;
        out[i + 1] = (-38*r -  74*g + 112*b + 128)/256 + 128;
        out[i + 0] = (112*r -  94*g -  18*b + 128)/256 + 128;
    }
}

bool normalize(QImage &img);
bool equalize(QImage &img);
bool mean(QImage &img);
