/*
 * Main window
 * SPDX-FileCopyrightText: 2021 Martin Sandsmark <martin.sandsmark@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "Viewer.h"

#include "HSL.h"

#include "ImageEffects.h"
#include "SmartCrop.h"

#include <QKeyEvent>
#include <QPainter>
#include <QScreen>
#include <QDebug>
#include <QImageReader>
#include <QGuiApplication>
#include <QBuffer>
#include <QMetaEnum>
#include <QColorSpace>
#include <QElapsedTimer>
#include <QTimer>
#include <QString>
#include <QClipboard>
#include <QMimeData>
#include <QFileInfo>
#include <QDir>
#include <QCollator>
#include <QMimeDatabase>

static constexpr const int s_histogramHeight = 200;

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
#include <xcb/xcb.h>
#else
#include <QX11Info>
#endif

#ifdef DEBUG_LOAD_TIME
#include <QElapsedTimer>
#endif//DEBUG_LOAD_TIME

extern "C" {
#include <xcb/xcb_icccm.h>
#include <unistd.h>
}

const bool s_printTiming = qEnvironmentVariableIsSet("QEH_BENCHMARK");

static const QString s_helpText = QStringLiteral(
        "?:\tShow/hide this message\n"
        "=/+/↑:\tZooms in\n"
        "-/↓:\tZooms out\n"
        "Right:\tMove right\n"
        "Left:\tMove left\n"
        "PgUp:\tPrevious image in dir\n"
        "PgDwn:\tNext image in dir\n"
        "Esc/Q:\tQuit\n"
        "F:\tMaximize\n"
        "I:\tShow/hide image info\n"
        "1-0:\tZoom 10-100%\n"
        "Space:\tToggle animation\n"
        "W:\tSpeed up animation\n"
        "S:\tSlow down animation\n"
        "D:\tStep animation forward\n"
        "A:\tStep animation backward\n"
        "CTRL+A:\tToggle smooth scaling\n"
        "Backspace:\tReset\n"
        "R:\tReload at current size (for vector formats)\n"
        "B:\tFill transparent background\n"
        "CTRL+←/→:\tRotate 90 degrees CCW or CW.\n"
        "ALT+←/→:\tStep animation backward/forward.\n"
        "CTRL+C:\tCopy image to clipboard\n"
        "CTRL+V:\tLoad image from clipboard\n"
        "E:\tEqualize\n"
        "N:\tNormalize\n"
        "Shift+H/S/L/V:\tVisualize hue/saturation/lightness/value.\n"
        "Shift+C:\tFake colorize with HSV."
    );


Viewer::Viewer()
{
    qRegisterMetaType<QImageReader::ImageReaderError>("QImageReader::ImageReaderError");
    setFlag(Qt::Dialog);

    m_background = QPixmap(20, 20);
    QPainter pmp(&m_background);
    pmp.fillRect(0, 0, 10, 10, Qt::lightGray);
    pmp.fillRect(10, 10, 10, 10, Qt::lightGray);
    pmp.fillRect(0, 10, 10, 10, Qt::darkGray);
    pmp.fillRect(10, 0, 10, 10, Qt::darkGray);
}

bool Viewer::load(const QString &filename)
{
    QElapsedTimer t; t.start();

    QIODevice *device = nullptr;
    if (filename == "-") {
        QFile stdinFile;
        stdinFile.open(STDIN_FILENO, QIODevice::ReadOnly, QFileDevice::DontCloseHandle);

        m_buffer = stdinFile.readAll();
        if (m_buffer.isEmpty()) {
            qDebug() << "No data from stdin";
            return false;
        }
        device = new QBuffer(&m_buffer, this);
    } else {
        m_fileName = filename;
        device = new QFile(filename, this);
    }

    if (!device->open(QIODevice::ReadOnly)) {
        qWarning() << "Failed to open" << filename << device->errorString();
        device->deleteLater();
        return false;
    }

    QImageReader reader(device);

    if (!reader.canRead()) {
        m_error = reader.error();
        qWarning().noquote() << "Error trying to read image from" << filename << ":" << reader.errorString();
    }
    m_format = reader.format();
    if (!reader.subType().isEmpty()) {
        m_format += "/" + reader.subType();
    }
    if (s_printTiming) qDebug() << "Read setup in" << t.restart() << "ms";

    if (reader.supportsOption(QImageIOHandler::ImageTransformation)) {
        m_transforms = reader.transformation();
        m_hasTransforms = true;
    }
    reader.setAutoTransform(true);
    if (s_printTiming) qDebug() << "Transform read in" << t.restart() << "ms";

    m_imageSize = reader.size();
    if (s_printTiming) qDebug() << "Size read in" << t.restart() << "ms";

    if (reader.supportsAnimation()) {
        static const QSet<QByteArray> brokenFormats = {
            "mng"
        };
        reader.setDevice(nullptr);
        device->deleteLater();

        resetMovie();
        m_brokenFormat = brokenFormats.contains(m_animation.format());
        if (m_brokenFormat) {
            qWarning() << m_animation.format() << "has issues, playback might get janky";
        }

        if (m_animation.currentImageSize().isValid()) {
            m_imageSize = m_animation.currentImageSize();
        }
        if (!m_imageSize.isValid()) {
            qWarning() << "Failed to get size from animated image";
            return false;
        }
        connect(&m_animation, &Animation::nextFrameReady, this, [this](QImage image) {
            m_image = image;
            m_histogram.initialized = false;
            updateScaled();
            update();
        });
    } else {
        reader.read(&m_image);
        reader.setDevice(nullptr);
        device->deleteLater();
        if (m_image.isNull()) {
            qWarning() << "Image reader error:" << reader.errorString();
            return false;
        }
        m_imageSize = m_image.size();
    }
    if (m_imageSize.isEmpty()) {
        return false;
    }
    m_scaledSize = m_imageSize;
    if (s_printTiming) qDebug() << "Image loaded in" << t.elapsed() << "ms";
    QSize minSize = m_imageSize;
    minSize.scale(100, 100, Qt::KeepAspectRatio);
    setMinimumSize(minSize);

    QSize maxSize = m_imageSize;
    maxSize.scale(screen()->availableSize() * 2, Qt::KeepAspectRatioByExpanding);
    setMaximumSize(maxSize);

    updateSize(m_imageSize, true);

    return true;
}

void Viewer::resetMovie()
{
    m_decodeSuccess = false;
    m_needReset = false;

    int speed = -1;
    QSize scaledSize;

    QIODevice *device = nullptr;
    if (!m_fileName.isEmpty()) {
        device = new QFile(m_fileName, this);
    } else {
        device = new QBuffer(&m_buffer, this);
    }
    device->open(QIODevice::ReadOnly);
    m_image = m_animation.load(device);

    // Completely arbitrary
    const bool bigImage = m_scaledSize.width()/1000 * m_scaledSize.height() / 1000 > 16;
    m_animation.cacheFrames = !bigImage || m_brokenFormat;

    m_animation.start();
}

void Viewer::updateSize(QSize newSize, bool initial)
{
    if (m_rotation % 180) {
        newSize = newSize.transposed();
    }

    QRect screenGeo = screen()->geometry();
    bool snapLeft = geometry().left() == screenGeo.left();
    bool snapRight = geometry().right() == screenGeo.right();

    const QSize maxSize = initial ? (screen()->availableSize() - QSize(20, 20)) : maximumSize();
    if (newSize.width() > maxSize.width() || newSize.height() >= maxSize.height()) {
        newSize.scale(maxSize.width(), maxSize.height(), Qt::KeepAspectRatio);
    }
    if (newSize.width() < minimumWidth()) {
        newSize.setWidth(minimumWidth());
    }
    if (newSize.height() < minimumHeight()) {
        newSize.setHeight(minimumHeight());
    }

    if (newSize == size()) {
        return;
    }
    QRect geo = geometry();
    const QPoint center = geo.center();
    geo.setSize(newSize);
    if (initial) {
        geo.moveCenter(screenGeo.center());
    } else {
        geo.moveCenter(center);
        if (snapLeft) {
            geo.moveLeft(screenGeo.left());
        } else if (snapRight) {
            geo.moveRight(screenGeo.right());
        }
    }

    setGeometry(geo);
}

void Viewer::ensureVisible()
{
    const QRect screenGeometry = screen()->availableGeometry();
    QRect newGeometry = geometry();
    if (geometry().left() > screenGeometry.right() - 50) {
        newGeometry.translate(-50, 0);
    } else if (geometry().right() < screenGeometry.left() + 50) {
        newGeometry.translate(50, 0);
    }
    if (geometry().top() > screenGeometry.bottom() - 50) {
        newGeometry.translate(0, -50);
    } else if (geometry().bottom() < screenGeometry.top() + 50) {
        newGeometry.translate(0, 50);
    }
    setPosition(newGeometry.topLeft());
}

void Viewer::setAspectRatio()
{
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    xcb_connection_t *connection = qGuiApp->nativeInterface<QNativeInterface::QX11Application>()->connection();
#else
    xcb_connection_t *connection = QX11Info::connection();
#endif

    xcb_size_hints_t hints;
    memset(&hints, 0, sizeof(hints));
    xcb_icccm_size_hints_set_aspect(&hints, m_imageSize.width(), m_imageSize.height(), m_imageSize.width(), m_imageSize.height());
    xcb_icccm_set_wm_normal_hints(connection, winId(), &hints);
}

template <typename ENUM> static const QString enumToString(const ENUM val)
{
    const QMetaEnum metaEnum = QMetaEnum::fromType<ENUM>();
    return metaEnum.valueToKey(static_cast<typename std::underlying_type<ENUM>::type>(val));
}
// Unfortunately not declared with Q_ENUM/Q_FLAG
QString transformationToString(const QImageIOHandler::Transformations transformations)
{
    const char *name = "";
    switch(transformations) {
    case QImageIOHandler::TransformationNone: return "None";
    case QImageIOHandler::TransformationMirror: return "Flipped horizontally";
    case QImageIOHandler::TransformationFlip: return "Flipped vertically";
    case QImageIOHandler::TransformationRotate180: return "Rotated 180⁰ (flipped vertically and horizontally)";
    case QImageIOHandler::TransformationRotate90: return "Rotated 90⁰ clockwise";
    case QImageIOHandler::TransformationMirrorAndRotate90: return "Flipped horizontally then rotated 90⁰";
    case QImageIOHandler::TransformationFlipAndRotate90: return "Flipped vertically then rotated 90⁰";
    case QImageIOHandler::TransformationRotate270: return "Rotated 270⁰ (flipped vertically, horizontally, rotated 90⁰)";
    default: return "Unknown (" + QString::number(transformations) + ")";
    }
}

void Viewer::paintEvent(QPaintEvent *event)
{
    if (s_printTiming && m_delayTimer.isValid()) {
        qDebug() << " - User input to paint:" << m_delayTimer.elapsed();
        m_delayTimer.invalidate();
    }

    if (startupTimer) {
        qDebug() << " - First onscreen paint:" << startupTimer->elapsed();
        startupTimer = nullptr;
    }

    QElapsedTimer t; t.start();

    const QRect rect(QPoint(0, 0), size());
    QRect visibleRect = QRect(mapToGlobal(QPoint(0, 0)), size()) & screen()->availableGeometry();
    visibleRect.moveTo(mapFromGlobal(visibleRect.topLeft()));
    QRect redrawRect = visibleRect & event->rect();
    if (redrawRect.isEmpty()) {
        return;
    }

    QPainter p(this);
    p.setClipRect(redrawRect);
    if (m_fillBackground) {
        p.fillRect(redrawRect, m_background);
    } else {
        p.setCompositionMode(QPainter::CompositionMode_Source);
    }

    QRect imageRect = m_scaled.rect();
    imageRect.moveCenter(rect.center());

    if (m_scaled.isNull()) {
        qWarning() << "Decode failure";
        return;
    }
    p.drawImage(redrawRect, m_scaled, redrawRect);

    // This _should_ always be empty
    const QRegion background = QRegion(redrawRect) - imageRect;
    for (const QRect &r : background) {
        p.fillRect(r, Qt::transparent);
    }

    p.setCompositionMode(QPainter::CompositionMode_SourceOver);
    if (m_showHistogram) {
        if (!m_histogram.initialized) {
            m_histogram.calculate32(m_image);
        }
        QRect histogramRect(0, 0, width(), s_histogramHeight);
        histogramRect.moveBottom(rect.bottom());
        p.fillRect(histogramRect, QColor(255, 255, 255, 32));
        p.drawImage(histogramRect, m_histogram.rendered(histogramRect.size()));
    }

    QString text;
    if (m_showHelp) {
        text = s_helpText;
    } else if (m_showInfo) {
        text += enumToString(m_image.format());
        text += "\nSize: " + QString::asprintf("%dx%d", m_imageSize.width(), m_imageSize.height());
        text += "\nFormat: " + m_format;
        if (m_hasTransforms) {
            text += "\nTransformations: " + transformationToString(m_transforms);
        }

        QColorSpace colors = m_image.colorSpace();
        if (colors.isValid() || m_image.colorCount()) {
            text += "\nColors:";
            if (colors.gamma()) {
                text += "\n  Gamma: " + QString::number(colors.gamma());
            }
            if (colors.isValid()) {
                text += "\n  Primaries: " + enumToString(colors.primaries());
                text += "\n  Transfer function: " + enumToString(colors.transferFunction());
            }
            if (m_image.colorCount()) {
                text += "\n  Color count: " + QString::number(m_image.colorCount());
            }
        }

        if (!m_image.textKeys().isEmpty()) {
            text += "\nMetadata:";
            for (const QString &key : m_image.textKeys()) {
                QString value = m_image.text(key).simplified();
                if (value.length() > 50) {
                    value = value.mid(0, 50) + "...";
                }
                text += "\n - " + key + ": " + value;
            }
        }

        if (m_animation) {
            text += "\nFrame: " + QString::number(m_animation.currentImageNumber());
            if (m_animation.frameCount()) {
                text += "/" + QString::number(m_animation.frameCount());
            }
            if (m_animation.speed != 100) {
                text += "\nSpeed: " + QString::number(m_animation.speed) + "%";
            }
            text += "\n" + enumToString(m_animation.state);
        }
    }
    if (!text.isEmpty()) {
        QRect textRect = p.boundingRect(rect, Qt::TextExpandTabs, text);
        textRect += QMargins(2, 2, 2, 2);
        textRect.moveTo(0, 0);
        p.fillRect(textRect, QColor(0, 0, 0, 128));
        p.setPen(Qt::white);
        p.drawText(textRect, Qt::TextExpandTabs, text);
    }
    if (s_printTiming && t.elapsed()) qDebug() << " - Paint done in" << t.restart() << "ms";

    if (m_animation) {
        m_animation.onPainted();
    }
}


void Viewer::wheelEvent(QWheelEvent *event)
{
    const qreal numDegrees = event->angleDelta().y();
    if (!numDegrees) {
        return;
    }
    const qreal delta = 1. + qAbs(numDegrees / 800.);
    if (numDegrees > 0) {
        updateSize(m_scaledSize * delta);
    } else {
        updateSize(m_scaledSize / delta);
    }
}

void Viewer::loadClipboard()
{
    QClipboard *clip = qGuiApp->clipboard();
    QImage clipboardImage = clip->image();
    if (clipboardImage.isNull()) {
        const QList<QUrl> urls = clip->mimeData()->urls();
        if (urls.isEmpty() || !urls.first().isLocalFile()) {
            return;
        }
        const QString path = urls.first().toLocalFile();
        if (load(path)) {
            qApp->setApplicationDisplayName(QFileInfo(path).fileName());
        }
        qDebug() << "Unhandled clipboard contents" << clip->mimeData()->formats();
        return;
    }
    m_animation.load(nullptr);
    m_imageSize = clipboardImage.size();
    m_image = clipboardImage;
    m_format = "clipboard";
    m_fileName = "";
    qApp->setApplicationDisplayName("Clipboard image");
    updateSize(m_imageSize);
    updateScaled();
}

void Viewer::loadNext(bool backwards)
{
    QFileInfo fi(m_fileName);
    if (!fi.exists()) {
        return;
    }
    QDir dir = fi.dir();
    if (m_fileList.isEmpty()) {
        QSet<QString> supportedTypes;
        for (const QByteArray &type : QImageReader::supportedMimeTypes()) {
            supportedTypes.insert(type);
        }
        QMimeDatabase db;
        for (const QFileInfo &fi : dir.entryInfoList(QDir::Files)) {
            if (supportedTypes.contains(db.mimeTypeForFile(fi).name())) {
                m_fileList.append(fi.fileName());
            }
        }

        QCollator comparer;
        comparer.setCaseSensitivity(Qt::CaseInsensitive);
        comparer.setNumericMode(true);

        std::sort(m_fileList.begin(), m_fileList.end(), [&](const QString &a, const QString &b) {
            return comparer.compare(a, b) < 0;
        });
    }
    if (m_fileList.isEmpty()) {
        qWarning() << "No other files available";
        return;
    }
    int index = m_fileList.indexOf(fi.fileName());
    if (index == -1) {
        qWarning() << "Can't find current filename" << m_fileName;
        index = 0;
    }
    index += backwards ? -1 : 1;
    if (index < 0) {
        index = m_fileList.size() - 1;
    } else if (index >= m_fileList.size()) {
        index = 0;
    }
    qDebug() << "Loading" << m_fileList[index] << index << "/" << m_fileList.size();
    m_animation.load(nullptr);
    load(dir.absoluteFilePath(m_fileList[index]));
    updateScaled();
    update();
}

void Viewer::keyPressEvent(QKeyEvent *event)
{
    m_delayTimer.restart();
    if (event->modifiers() & Qt::ShiftModifier) {
        switch(event->key()) {
        case Qt::Key_L:
            toggleEffect(Lightness);
            return;
        case Qt::Key_V:
            toggleEffect(Value);
            return;
        case Qt::Key_H:
            toggleEffect(Hue);
            return;
        case Qt::Key_S:
            toggleEffect(Saturation);
            return;
        case Qt::Key_C:
            toggleEffect(Hsv);
            return;
        default:
            break;
        }
    }

    QSize screenSize = screen()->availableSize();
    if (m_rotation % 180) {
        screenSize = screenSize.transposed();
    }
    QSize fullSize = m_imageSize.scaled(screenSize, Qt::KeepAspectRatio);
    switch(event->key()) {
    case Qt::Key_1: updateSize(fullSize  * 0.1); return;
    case Qt::Key_2: updateSize(fullSize  * 0.2); return;
    case Qt::Key_3: updateSize(fullSize  * 0.3); return;
    case Qt::Key_4: updateSize(fullSize  * 0.4); return;
    case Qt::Key_5: updateSize(fullSize  * 0.5); return;
    case Qt::Key_6: updateSize(fullSize  * 0.6); return;
    case Qt::Key_7: updateSize(fullSize  * 0.7); return;
    case Qt::Key_8: updateSize(fullSize  * 0.8); return;
    case Qt::Key_9: updateSize(fullSize  * 0.9); return;
    case Qt::Key_0: updateSize(m_imageSize); return;
    case Qt::Key_Backspace: {
        m_animation.speed = 100;
        m_rotation = 0;
        m_effect = None;
        updateScaled();
        const QRect screenGeo = screen()->availableGeometry();
        QRect geo = geometry();
        geo.moveCenter(screenGeo.center());
        setGeometry(geo);
        updateSize(m_imageSize, true);
        update();
        return;
    }
    case Qt::Key_Space:
        if (!m_animation) {
            return;
        }
        if (m_animation.state == Animation::Playing) {
            m_animation.pause();
        } else {
            m_animation.start();
        }
        return;
    case Qt::Key_W:
        m_animation.speed *= 1.1;
        return;
    case Qt::Key_S:
        m_animation.speed /= 1.1;
        break;
    case Qt::Key_A:
        if (event->modifiers() & Qt::ControlModifier) {
        m_smooth = !m_smooth;
        updateScaled();
        update();
            return;
        }
        if (!m_animation) {
            return;
        }
        m_animation.pause();
        if (m_animation.currentImageNumber() == 0) {
            m_animation.jumpToImage(m_animation.frameCount() - 1);
        } else {
            m_animation.jumpToImage(m_animation.currentImageNumber() - 1);
        }
        break;
    case Qt::Key_D:
        if (!m_animation) {
            return;
        }
        m_animation.pause();
        if (m_animation.frameCount() && m_animation.currentImageNumber() >= m_animation.frameCount()) {
            m_animation.jumpToImage(0);
        } else {
            m_animation.jumpToImage(m_animation.currentImageNumber() + 1);
        }
        break;
    case Qt::Key_H:
        m_showHistogram = !m_showHistogram;
        update();
        break;
    case Qt::Key_N:
        toggleEffect(Normalize);
        break;
    case Qt::Key_E:
        toggleEffect(Equalize);
        break;
    case Qt::Key_Y:
        toggleEffect(YCbCr);
        break;
    case Qt::Key_M:
        toggleEffect(Mean);
        break;
    case Qt::Key_Equal:
    case Qt::Key_Plus:
    case Qt::Key_Up:
        if (event->modifiers()) {
            updateSize(m_scaledSize * 2);
        } else {
            updateSize(m_scaledSize * 1.1);
        }
        return;
    case Qt::Key_PageUp:
        loadNext(true);
        return;
    case Qt::Key_Minus:
    case Qt::Key_Down:
        if (event->modifiers()) {
            updateSize(m_scaledSize / 2);
        } else {
            updateSize(m_scaledSize / 1.1);
        }
        return;
    case Qt::Key_PageDown:
        loadNext(false);
        return;
    case Qt::Key_F: {
        if (m_imageSize.width() < screenSize.width() && m_imageSize.height() < screenSize.width()) {
            if (width() >= screenSize.width() || height() >= screenSize.height()) {
                updateSize(m_imageSize);
            } else {
                updateSize(fullSize);
            }
        } else {
            if (width() > fullSize.width() || height() > fullSize.height()) {
                updateSize(fullSize, true);
            } else {
                updateSize(m_imageSize);
            }

        }
        return;
    }
    case Qt::Key_B:
        m_fillBackground = !m_fillBackground;
        update();
        break;
    case Qt::Key_R: {
        if (m_animation) {
            break;
        }
        QIODevice *device = nullptr;
        if (!m_buffer.isEmpty()) {
            device = new QBuffer(&m_buffer, this);
        } else if (!m_fileName.isEmpty()) {
            device = new QFile(m_fileName, this);
        } else {
            break;
        }
        QImageReader reader(device);
        reader.setAutoTransform(true);
        reader.setScaledSize(m_scaledSize);
        m_image = reader.read();
        device->deleteLater();
        updateScaled();
        update();
        break;
    }
    case Qt::Key_Q:
    case Qt::Key_Escape:
        close();
        return;
    case Qt::Key_C:
        if (event->modifiers() & Qt::ControlModifier) {
            qGuiApp->clipboard()->setImage(m_scaled);
        }
        break;
    case Qt::Key_V: {
        if (!(event->modifiers() & Qt::ControlModifier)) {
            break;
        }
        loadClipboard();
        break;
    }
    case Qt::Key_Right: {
        QRect geom = geometry();
        if (event->modifiers() & Qt::ControlModifier) {
            m_rotation = (m_rotation + 90) % 360;
            updateSize(m_scaledSize);
            return;
        }
        const QRect screenGeo = screen()->availableGeometry();
        if (geom.left() == screenGeo.left()) {
            geom.moveCenter(screenGeo.center());
        } else {
            geom.moveRight(screenGeo.right());
        }
        setGeometry(geom);
        break;
    }
    case Qt::Key_Left: {
        if (event->modifiers() & Qt::ControlModifier) {
            m_rotation = (m_rotation - 90) % 360;
            updateSize(m_scaledSize);
            break;
        }
        QRect geom = geometry();
        const QRect screenGeo = screen()->availableGeometry();
        if (geom.right() == screenGeo.right()) {
            geom.moveCenter(screenGeo.center());
        } else {
            geom.moveLeft(screenGeo.left());
        }
        setGeometry(geom);
        break;
    }
    case Qt::Key_I:
        m_showInfo = !m_showInfo;
        update();
        break;
    case Qt::Key_Question:
        m_showHelp = !m_showHelp;
        update();
        break;
    default:
        break;
    }
    QRasterWindow::keyPressEvent(event);
}

void Viewer::updateScaled()
{
    if (m_imageSize.isEmpty()) {
        return;
    }
    QElapsedTimer t; t.start();

    QSize windowSize = size();
    QSize imageSize = m_imageSize;
    if (m_rotation % 180) {
        windowSize.transpose();
        imageSize.transpose();
    }
    imageSize = imageSize.scaled(windowSize, Qt::KeepAspectRatioByExpanding);

    const bool scalingUp = m_image.width() < windowSize.width();
    const bool transpose = (m_rotation % 180) != 0;

    m_scaled = m_image;

    int sizeDelta = qMax(qAbs(windowSize.width() - imageSize.width()), qAbs(windowSize.height() - imageSize.height()));
    if (imageSize != windowSize) {
        m_scaled = SmartCrop::crop(m_image, windowSize);
    }

    if (scalingUp) {
        if (m_scaled.colorSpace() != QColorSpace::SRgb) {
            m_scaled.convertToColorSpace(QColorSpace::SRgb);
        }

        if (m_effect == Equalize) {
            equalize(m_scaled);
        } else if (m_effect == Normalize) {
            normalize(m_scaled);
        } else if (m_effect == YCbCr) {
            toYCbCr(&m_scaled);
        } else if (m_effect == Mean) {
            mean(m_scaled);
        } else if (m_effect <= Hsv) {
            m_scaled = extractHsl(m_scaled, int(m_effect));
        }

        if (s_printTiming && m_effect != None) {
            qDebug() << " - Effect pre applied in" << t.restart() << "ms";
        }

        if (m_rotation != 0) {
            QTransform transform;
            transform.rotate(m_rotation);
            m_scaled = m_scaled.transformed(transform);
        }
    }
    const Qt::TransformationMode mode = m_smooth ? Qt::SmoothTransformation : Qt::FastTransformation;
    m_scaled = m_scaled.scaled(scalingUp ? size() : windowSize, Qt::KeepAspectRatio, mode);

    if (s_printTiming) {
        if (t.elapsed()) {
            qDebug() << " - Scaled in" << t.elapsed() << "ms";
        }
        t.restart();
    }

    if (!scalingUp) {
        if (m_scaled.colorSpace() != QColorSpace::SRgb) {
            m_scaled.convertToColorSpace(QColorSpace::SRgb);
        }

        if (m_effect == Equalize) {
            equalize(m_scaled);
        } else if (m_effect == Normalize) {
            normalize(m_scaled);
        } else if (m_effect == YCbCr) {
            toYCbCr(&m_scaled);
        } else if (m_effect == Mean) {
            mean(m_scaled);
        } else if (m_effect <= Hsv) {
            m_scaled = extractHsl(m_scaled, int(m_effect));
        }

        if (s_printTiming && m_effect != None) {
            qDebug() << " - Effect post applied in" << t.elapsed() << "ms";
        }

        if (m_rotation != 0) {
            QTransform transform;
            transform.rotate(m_rotation);
            m_scaled = m_scaled.transformed(transform);
        }
    }
    QString zoomString = QString::number(100 * m_scaledSize.width() / m_imageSize.width()) + "%";
    if (m_fileName.isEmpty()) {
        setTitle(zoomString);
    } else {
        setTitle(m_fileName + " (" + zoomString + ")");
    }
}

void Viewer::resizeEvent(QResizeEvent *event)
{
    QSize imageSize = m_imageSize;
    QSize windowSize = size();
    if (m_rotation % 180) {
        windowSize.transpose();
    }
    m_scaledSize = imageSize.scaled(windowSize, Qt::KeepAspectRatio);
    updateScaled();
    QMetaObject::invokeMethod(this, &Viewer::setAspectRatio, Qt::QueuedConnection);
    QRasterWindow::resizeEvent(event);
    QMetaObject::invokeMethod(this, &Viewer::ensureVisible, Qt::QueuedConnection);
    update();
}

void Viewer::moveEvent(QMoveEvent *event)
{
    QRasterWindow::moveEvent(event);

    QRegion newVisible = QRect(event->pos(), size()) & screen()->availableGeometry();
    QRegion oldVisible = QRect(event->oldPos(), size()) & screen()->availableGeometry();
    newVisible.translate(-event->pos());
    oldVisible.translate(-event->oldPos());
    update(newVisible ^ oldVisible);

    QMetaObject::invokeMethod(this, &Viewer::ensureVisible, Qt::QueuedConnection);
}

void Viewer::mousePressEvent(QMouseEvent *ev)
{
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    m_lastMousePos = ev->globalPosition();
#else
    m_lastMousePos = ev->globalPos();
#endif
    QGuiApplication::setOverrideCursor(QCursor(Qt::ClosedHandCursor));
}

void Viewer::mouseReleaseEvent(QMouseEvent *)
{
    QGuiApplication::restoreOverrideCursor();
}

void Viewer::mouseMoveEvent(QMouseEvent *ev)
{
    if (!ev->buttons()) {
        return;
    }
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    const QPointF globalPos = ev->globalPosition().toPoint();
#else
    const QPoint globalPos = ev->globalPos();
#endif
    const QPointF delta = globalPos - m_lastMousePos;
    setPosition((position() + delta).toPoint());
    m_lastMousePos = globalPos;
}

bool Viewer::event(QEvent *ev)
{
    switch(ev->type()) {
    case QEvent::UpdateRequest:
        return QRasterWindow::event(ev);
    case QEvent::FocusIn:
    case QEvent::FocusOut:
        // Avoid QWindow connecting to dbus
        ev->accept();
        return true;
    default:
        return QRasterWindow::event(ev);
    }
}
