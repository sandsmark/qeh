#pragma once

#include <QImage>
#include <QPolygon>
#include <QElapsedTimer>
#include <QPainter>
#include <QDebug>
#include <QtMath>

extern const bool s_printTiming;
struct Histogram
{
    int red[256];
    int green[256];
    int blue[256];

    void calculate32(const QImage &image)
    {
        QElapsedTimer t; t.start();
        if (image.depth() != 32) {
            qWarning() << "Invalid format" << image.format();
            if (image.hasAlphaChannel()) {
                calculate32(image.convertToFormat(QImage::Format_ARGB32));
            } else {
                calculate32(image.convertToFormat(QImage::Format_RGB32));
            }
            return;
        }

        bzero(red, sizeof red);
        bzero(green, sizeof green);
        bzero(blue, sizeof blue);

        const int height = image.height();
        const int width = image.width();
        for (int y=0; y<height; y++) {
            const QRgb *rgb = (const QRgb*)(image.constScanLine(y));
            for (int x=0; x<width; x++) {
                red[qRed(rgb[x])]++;
                green[qGreen(rgb[x])]++;
                blue[qBlue(rgb[x])]++;
            }
        }

        m_maxY = 0;
        for (int i=0; i<256; i++) {
            m_maxY = qMax(red[i], qMax(green[i], qMax(blue[i], m_maxY)));
        }

        // Flip it, makes rendering below easier
        for (int i=0; i<256; i++) {
            red[i] = m_maxY - red[i];
            green[i] = m_maxY - green[i];
            blue[i] = m_maxY - blue[i];
        }

        //if (t.elapsed()) qDebug() << "Calculated in" << t.elapsed() << "ms";
        initialized = true;
        m_needRender = true;;
    }
    bool initialized = false;

    const QImage &rendered(const QSize &size)
    {
        if (size == m_rendered.size() && !m_needRender) {
            return m_rendered;
        }
        QElapsedTimer t; t.start();

        const qreal height = size.height();
        const qreal width = size.width();

        // Start and end points
        redPoly[0] = greenPoly[0] = bluePoly[0] = QPoint(0, height);
        redPoly[257] = greenPoly[257] = bluePoly[257] = QPoint(width, height);

        const qreal multY = height / m_maxY;

        QPoint *rp = &redPoly[1];
        QPoint *gp = &greenPoly[1];
        QPoint *bp = &bluePoly[1];

        for (int i=0; i<256; i++) {
            const qreal x = i * width / 256.;
            rp[i] = QPoint(x, red[i] * multY);
            gp[i] = QPoint(x, green[i] * multY);
            bp[i] = QPoint(x, blue[i] * multY);
        }

        if (m_rendered.size() != size) {
            m_rendered = QImage(size, QImage::Format_ARGB32_Premultiplied);
        }
        m_rendered.fill(Qt::transparent);

        QPainter p(&m_rendered);
        p.setCompositionMode(QPainter::CompositionMode_Plus);
        p.setRenderHint(QPainter::Antialiasing);
        p.setPen(Qt::transparent);

        p.setBrush(Qt::red);
        p.drawConvexPolygon(redPoly, 258);

        p.setBrush(Qt::green);
        p.drawConvexPolygon(greenPoly, 258);

        p.setBrush(Qt::blue);
        p.drawConvexPolygon(bluePoly, 258);

        p.end();

        if ((t.elapsed() > 0 && s_printTiming) || t.elapsed() > 2) qDebug() << "Histogram rendered in" << t.elapsed() << "ms";

        m_needRender = false;

        return m_rendered;
    }
private:
    QImage m_rendered;
    bool m_needRender = true;
    int m_maxY = 0;
    QPoint redPoly[258];
    QPoint greenPoly[258];
    QPoint bluePoly[258];
};

