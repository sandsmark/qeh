/*
 * SPDX-FileCopyrightText: 2021 Martin Sandsmark <martin.sandsmark@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#pragma once
#include <QImage>
enum HSVL {
    Hue = 0,
    Saturation = 1,
    Value = 2,
    Lightness = 3,
};
QImage extractHsl(QImage &input, int what);
