/*
 * SPDX-FileCopyrightText: 2021 Martin Sandsmark <martin.sandsmark@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "HSL.h"
#include <QDebug>
#include <limits>

template<typename T, typename PXL>
struct HSL
{
    T h = 0;
    T s = 0;
    T v = 0;
    T l = 0;

    T max, min, delta;
    uchar whatmax;
    static constexpr T satMult = (std::numeric_limits<PXL>::max() * 510) / std::numeric_limits<uint8_t>::max();

    inline void convert(T r, T g, T b)
    {
        h = 0;
        max = r;                               // maximum RGB component
        whatmax = 0;                            // r=>0, g=>1, b=>2
        if(g > max) { max = g; whatmax = 1; }
        if(b > max) { max = b; whatmax = 2; }
        min = r;                               // find minimum value
        if(g < min) min = g;
        if(b < min) min = b;
        delta = max-min;
        v = max;                                   // calc value
        l = (max + min) / 2;
        s = max ? (satMult*delta+max)/(2*max) : 0;
        if(s == 0) {
            h = 0;                                // undefined hue
        } else{
            switch(whatmax){
                case 0:                             // red is max component
                    h = (g >= b) ? (120*(g-b)+delta)/(2*delta) :
                         300 + (120*(g-b+delta)+delta)/(2*delta);
                    break;
                case 1:                             // green is max component
                    h = (b > r) ? 120 + (120*(b-r)+delta)/(2*delta) :
                        60 + (120*(b-r+delta)+delta)/(2*delta);
                    break;
                case 2:                             // blue is max component
                    h = (r > g) ? 240 + (120*(r-g)+delta)/(2*delta) :
                        180 + (120*(r-g+delta)+delta)/(2*delta);
                    break;
            }
        }
    }
};

template<typename T>
QImage actualExtractHsl(QImage &input, int what)
{
    HSL<int64_t, T> hsl;
    QImage ret;
    switch(what) {
    default: {
        ret = QImage(input.size(), input.format());
        for (int y=0; y<input.height(); y++) {
            T *target = (T*)ret.scanLine(y);
            const T *source = (const T*)input.constScanLine(y);
            for (int x=0; x < input.width(); x++) {
                hsl.convert(source[x * 4 + 0], source[x * 4 + 1], source[x * 4 + 2]);
                target[x * 4 + 1] = hsl.v;
                target[x * 4 + 2] = hsl.s;
                target[x * 4 + 0] = std::numeric_limits<T>::max() * hsl.h / 180;
                target[x * 4 + 3] = std::numeric_limits<T>::max();
            }
        }
        break;
    }
    case Hue: {
        if (sizeof(T) == 2) {
            ret = QImage(input.size(), QImage::Format_Grayscale16);
        } else {
            ret = QImage(input.size(), QImage::Format_Grayscale8);
        }
        for (int y=0; y<input.height(); y++) {
            T *target = (T*)ret.scanLine(y);
            const T *source = (const T*)input.constScanLine(y);
            for (int x=0; x < input.width(); x++) {
                hsl.convert(source[x * 4 + 0], source[x * 4 + 1], source[x * 4 + 2]);
                target[x] = std::numeric_limits<T>::max() * hsl.h / 180;
            }
        }
        break;
    }
    case Saturation: {
        if (sizeof(T) == 2) {
            ret = QImage(input.size(), QImage::Format_Grayscale16);
        } else {
            ret = QImage(input.size(), QImage::Format_Grayscale8);
        }
        for (int y=0; y<input.height(); y++) {
            T *target = (T*)ret.scanLine(y);
            const T *source = (const T*)input.constScanLine(y);
            for (int x=0; x < input.width(); x++) {
                hsl.convert(source[x * 4 + 0], source[x * 4 + 1], source[x * 4 + 2]);
                target[x] = hsl.s;
            }
        }
        break;
    }
    case Value: {
        if (sizeof(T) == 2) {
            ret = QImage(input.size(), QImage::Format_Grayscale16);
        } else {
            ret = QImage(input.size(), QImage::Format_Grayscale8);
        }
        for (int y=0; y<input.height(); y++) {
            T *target = (T*)ret.scanLine(y);
            const T *source = (const T*)input.constScanLine(y);
            for (int x=0; x < input.width(); x++) {
                hsl.convert(source[x * 4 + 0], source[x * 4 + 1], source[x * 4 + 2]);
                target[x] = hsl.v;
            }
        }
        break;
        }
    case Lightness: {
        if (sizeof(T) == 2) {
            ret = QImage(input.size(), QImage::Format_Grayscale16);
        } else {
            ret = QImage(input.size(), QImage::Format_Grayscale8);
        }
        for (int y=0; y<input.height(); y++) {
            T *target = (T*)ret.scanLine(y);
            const T *source = (const T*)input.constScanLine(y);
            for (int x=0; x < input.width(); x++) {
                hsl.convert(source[x * 4 + 0], source[x * 4 + 1], source[x * 4 + 2]);
                target[x] = hsl.l;
            }
        }
        break;
    }
    }
    return ret;
}


QImage extractHsl(QImage &input, int what)
{
    if (input.depth() == 64) {
        if (input.format() == QImage::Format_RGBA64_Premultiplied) {
            input = input.convertToFormat(QImage::Format_RGBA64);
        }
        return actualExtractHsl<uint16_t>(input, what);
    }
    if (input.depth() != 32) {
        input = input.convertToFormat(input.hasAlphaChannel() ?
                                  QImage::Format_ARGB32 :
                                  QImage::Format_RGB32);
    } else if (input.format() == QImage::Format_ARGB32_Premultiplied) {
        input = input.convertToFormat(QImage::Format_ARGB32);
    }
    return actualExtractHsl<uint8_t>(input, what);
}

